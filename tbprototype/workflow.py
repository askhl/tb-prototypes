from abc import ABC, abstractmethod
from collections.abc import MutableMapping
from functools import cached_property


class Parametrize:
    def __init__(self, func, selection):
        self.func = func
        self.selection = selection

    def __iter__(self):
        for key, value in self.selection._dct.items():
            yield key, Task(self.func, thing=value)


class ParametrizeWorkflow:
    def __init__(self, workflow, selection):
        self.workflow = workflow
        self.selection = selection

    def __iter__(self):
        for key, value in self.selection._dct.items():
            # XXX enable user to specify passing of "material="
            workflow = self.workflow(key, material=value)
            yield key, workflow


class Task:
    def __init__(self, func, **kwargs):
        self.func = func
        self.kwargs = kwargs

    def run(self):
        return self.func(**self.kwargs)

    def description(self):
        kwargsstring = str(self.kwargs)
        if len(kwargsstring) > 20:
            kwargsstring = '{…}'
        return f'Task({self.func.__name__}(…), {kwargsstring})'

    def __tb_node__(self):
        return self


class BaseWorkflow(ABC):
    def __init__(self, name, eager=True):
        self.name = name
        self._dct = NestedNode()
        self.eager = eager

    @abstractmethod
    def __tb_subworkflows__(self):
        """Yield (name, object) tuples where object is task/workflow-like."""

    def run_recursive(self):
        # XXX need topological sort
        subworkflows = self.run()
        for name, subworkflow in subworkflows.items():
            if isinstance(subworkflow, BaseWorkflow):
                subworkflow.run_recursive()
        return self._dct

    @property
    def subnodes(self):
        return self._dct

    def description(self):
        return '<Generic workflow description>'

    def ls(self, indent=''):
        print(f'{indent}{self.name} {self.description()}')
        for name, subnode in self.subnodes.items():
            if isinstance(subnode, BaseWorkflow):
                subnode.ls(indent=indent + '    ')
            else:
                # assert isinstance(subnode, Task), subnode
                print(f'{indent}    {name} {subnode.description()}')

    def run(self, *, selection=None):
        if selection is None:
            selection = set()

        # For "very long" workflows like totree, the __tb_subworkflows__()
        # protocol could take selection as an input and translate that
        # into e.g. ASE database query.
        #
        # This also means the user would be calling totree specifically
        # as a command, and is able to specify arguments such as a query,
        # which will not interfere with other workflows since the user
        # is currently only running totree as a manual step.
        for name, thing in self.__tb_subworkflows__():
            if self.eager or name in self._dct or thing in selection:
                # This is a bit hacky.  We want all the stuff we loop over
                # to be normalized to a kind of "node", whether it's
                # MaterialsRowNode, Task, TaskDecorator, or BaseWorkflow.
                #
                # As of now, only BaseWorkflow is returns something
                # other than self.
                node = thing.__tb_node__()
                self._dct[name] = node

        return self._dct

    # XXX should add more methods (if this is the right place to do
    # name-based access)
    def __getitem__(self, name):
        tokens = name.split('.', 1)
        item = self._dct[tokens[0]]
        if len(tokens) == 2:
            item = item[tokens[1]]
        return item

    def __tb_node__(self):
        return self


class Workflow(BaseWorkflow):
    def __tb_subworkflows__(self):
        for name in dir(type(self)):
            obj = getattr(self, name)
            if isinstance(obj, (Subworkflow, TaskDecorator)):
                yield name, obj


class Subworkflow(BaseWorkflow):
    def __init__(self, meth, parent_workflow, eager):
        self.meth = meth
        self.parent_workflow = parent_workflow

        super().__init__(meth.__name__, eager)

    def description(self):
        parent_typename = self.parent_workflow.__class__.__name__
        qualified_name = f'{parent_typename}.{self.meth.__name__}'
        return f'{qualified_name}() eager={self.eager}'

    def __tb_subworkflows__(self):
        thing = self.meth(self.parent_workflow)
        if hasattr(thing, '__tb_subworkflows__'):
            return thing.__tb_subworkflows__()

        # The decorated thing was a generator already, just return it
        return thing

    def __repr__(self):
        return f'Subworkflow(<{self.description()}>)'


def subworkflow(eager):
    def decorate(func):
        return cached_property(lambda instance: Subworkflow(
            func, instance, eager))
    return decorate


def task(workertype=None):
    def decorate(meth):
        return cached_property(lambda instance: TaskDecorator(
            meth, instance, workertype))
    return decorate


class TaskDecorator:
    def __init__(self, meth, instance, workertype):
        self.meth = meth
        self.instance = instance
        self.workertype = workertype

    def description(self):
        return f'TaskDecorator({self.meth.__name__})'

    def __tb_node__(self):
        return self.meth(self.instance)

    @property
    def output(self):
        return OutputPointer(self)


class OutputPointer:
    def __init__(self, taskdecorator):
        self.taskdecorator = taskdecorator


class NestedNode(MutableMapping):
    def __init__(self):
        self._dct = {}

    def __len__(self):
        return len(self._dct)

    def __iter__(self):
        return iter(self._dct)

    def __getitem__(self, key):
        return self._dct[key]

    def __setitem__(self, key, value):
        self._dct[key] = value

    def __delitem__(self, key):
        del self._dct[key]

    def __repr__(self):
        return f'NestedNode({self._dct})'
