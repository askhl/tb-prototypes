from dataclasses import dataclass
import numpy as np
import matplotlib.pyplot as plt


class Reference:
    def __init__(self, formula, hform, label=None, legend=None):
        from ase.formula import Formula
        self._formula = Formula(formula)
        self.formula = str(self._formula)
        self.natoms = len(self._formula)
        self.hform = hform
        if label is None:
            label = formula
        self.label = label
        if legend is None:
            legend = 'hello'
        self.legend = legend

    def total_energy(self):
        return self.natoms * self.hform

    def __repr__(self):
        return f'{self.formula} {self.natoms} {self.hform}'


def get_hull_energies(pd):
    hull_energies = []
    for ref in pd.references:
        count = ref[0]
        refenergy = ref[1]
        natoms = ref[3]
        decomp_energy, indices, coefs = pd.decompose(**count)
        ehull = (refenergy - decomp_energy) / natoms
        hull_energies.append(ehull)

    return hull_energies


def calculate_hull(references):
    from ase.phasediagram import PhaseDiagram

    # ase pd compatible format of "references":
    pdrefs = [(reference.formula, reference.total_energy())
              for reference in references]

    pd = PhaseDiagram(pdrefs, verbose=False)

    hull_energies = get_hull_energies(pd)

    # XXX wtf, this doesn't actually plot anything it only returns more values
    xcoord, energies, _, hull, simplices, _, _ = pd.plot2d2()

    symbols = pd.symbols
    return StabilityDiagram(
        references, hull_energies, simplices,
        xcoord, energies, symbols)


@dataclass
class StabilityDiagram:
    references: list
    hull_energies: list
    simplices: list
    xcoord: list
    energies: list
    symbols: list

    def plot(self, ax):
        references = self.references
        hull_energies = self.hull_energies
        simplices = self.simplices
        xcoord = self.xcoord
        energy = self.energies
        symbols = self.symbols

        legends, sizes = self.get_legends_and_sizes()
        legendhandles = self.get_legendhandles(ax, legends)

        hull = np.array(hull_energies) < 0.05
        edgecolors = np.array(['C2' if hull_energy < 0.05 else 'C3'
                               for hull_energy in hull_energies])

        for i, j in simplices:
            ax.plot(xcoord[[i, j]], energy[[i, j]], '-', color='C0')
        names = [ref.label for ref in references]

        xcoord0 = xcoord[~hull]
        energy0 = energy[~hull]
        ax.scatter(
            xcoord0, energy0,
            facecolor='none', marker='o',
            edgecolor=np.array(edgecolors)[~hull], s=sizes[~hull],
            zorder=9)

        ax.scatter(
            xcoord[hull], energy[hull],
            facecolor='none', marker='o',
            edgecolor=np.array(edgecolors)[hull], s=sizes[hull],
            zorder=10)

        for a, b, name, on_hull in zip(xcoord, energy, names, hull):
            ax.text(a + 0.02, b, name, ha='left', va='center')

        ax.set_xlabel('{}$_{{1-x}}${}$_x$'.format(*symbols))
        ax.set_ylabel(r'$\Delta H$ [eV/atom]')

        # Circle this material
        ymin = energy.min()
        bottom_whitespace = 0.1 * energy.ptp()
        ax.axis(xmin=-0.1, xmax=1.1, ymin=ymin - bottom_whitespace)
        newlegendhandles = [(legendhandles[0], legendhandles[1]),
                            *legendhandles[2:]]

        ax.legend(
            newlegendhandles,
            [r'$E_\mathrm{h} {^</_>}\, 5 \mathrm{meV}$', *legends],
            loc='lower left',
            handler_map={tuple: ObjectHandler()})

    def get_legendhandles(self, ax, legends):
        legendhandles = []

        for it, label in enumerate(['On hull', 'off hull']):
            handle = ax.fill_between([], [],
                                     color=f'C{it + 2}', label=label)
            legendhandles.append(handle)

        for it, legend in enumerate(legends):
            handle = ax.scatter([], [], facecolor='none', marker='o',
                                edgecolor='k', label=legend, s=(3 + it * 3)**2)
            legendhandles.append(handle)
        return legendhandles

    def get_legends_and_sizes(self):
        legends = []
        sizes = []
        for reference in self.references:
            legend = reference.legend
            if legend and legend not in legends:
                legends.append(legend)
            if legend in legends:
                idlegend = legends.index(reference.legend)
                size = (3 * idlegend + 3)**2
            else:
                size = 2
            sizes.append(size)
        sizes = np.array(sizes)
        return legends, sizes


class ObjectHandler:
    def legend_artist(self, legend, orig_handle, fontsize, handlebox):
        from matplotlib import patches
        x0, y0 = handlebox.xdescent, handlebox.ydescent
        width, height = handlebox.width, handlebox.height
        patch = patches.Polygon(
            [
                [x0, y0],
                [x0, y0 + height],
                [x0 + 3 / 4 * width, y0 + height],
                [x0 + 1 / 4 * width, y0],
            ],
            closed=True, facecolor='C2',
            edgecolor='none', lw=3,
            transform=handlebox.get_transform())
        handlebox.add_artist(patch)
        patch = patches.Polygon(
            [
                [x0 + width, y0],
                [x0 + 1 / 4 * width, y0],
                [x0 + 3 / 4 * width, y0 + height],
                [x0 + width, y0 + height],
            ],
            closed=True, facecolor='C3',
            edgecolor='none', lw=3,
            transform=handlebox.get_transform())
        handlebox.add_artist(patch)
        return patch


def main():
    heat_of_formation = -0.78

    actual_material = Reference(
        'FeO2', heat_of_formation,
        label='FeO2 (potato)',
        legend='potato')

    references = [
        actual_material,
        Reference('Fe', 0.0),
        Reference('O8', 0.0),
        Reference('FeO', -0.87),
        Reference('Fe12O16', -1.10),
        Reference('FeO3', -1.14, label='FeO3 (onion)', legend='potato'),
    ]

    result = calculate_hull(references)

    ehull = result.hull_energies[0]  # corresponding to 0th element of refs
    print('energy above convex hull', ehull)
    assert abs(ehull - 0.3413333333333333) < 1e-10

    ax = plt.gca()
    result.plot(ax)

    # ax.plot(result.xcoord[0], result.energies[0],
    #        marker='o', markersize=10, ls=None, color='purple',
    #        markerfacecolor='none')

    plt.show()


if __name__ == '__main__':
    main()
