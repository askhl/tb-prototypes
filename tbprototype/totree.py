from tbprototype.emt import do_niggli_reduce, emt_relax_cell, emt_singlepoint
import tbprototype.workflow as tb


class MaterialsRowNode:
    # Custom user object which must be integrated with JSON/storage etc.

    def __init__(self, row):
        self._row = row
        self._atoms = row.toatoms()

    def toatoms(self):
        return self._atoms

    def identity(self):
        return self._row.unique_id

    def name(self):
        formula = self._atoms.symbols.formula.format('metal')
        return f'{formula}-{self.identity()}'

    def __repr__(self):
        return f'MaterialsRowNode({self.name()})'

    # XXX This must look like a node but maybe we need some special
    # type for "chunks of data"
    def description(self):
        return repr(self)

    def __tb_node__(self):
        return self

    @classmethod
    def totree(cls, dbfile):
        from ase.db import connect
        with connect(dbfile) as conn:
            for row in conn.select():
                rownode = cls(row)
                yield rownode.name(), rownode


class EMTWorkflow(tb.Workflow):
    def __init__(self, name, material):
        self.material = material
        super().__init__(name)

    @tb.task(workertype='small')
    def niggli(self):
        return tb.Task(do_niggli_reduce, material=self.material)

    @tb.task(workertype='big')
    def relax(self):
        return tb.Task(emt_relax_cell, atoms=self.niggli.output)

    @tb.task(workertype='big')
    def singlepoint(self):
        return tb.Task(emt_singlepoint, atoms=self.relax.output)

    @tb.subworkflow(eager=True)
    def stiffness(self):
        from tbprototype.stiffness import StiffnessWorkflow
        # (Should not need to pass name explicitly here.)
        return StiffnessWorkflow('stiffness', atoms=self.material._atoms)


class MasterWorkflow(tb.Workflow):
    def __init__(self, name, dbfile):
        self.dbfile = dbfile
        super().__init__(name)

    @tb.subworkflow(eager=False)
    def totree(self):
        return MaterialsRowNode.totree(self.dbfile)

    @tb.subworkflow(eager=True)
    def emt_relax(self):
        return tb.ParametrizeWorkflow(EMTWorkflow, self.totree)


def main():
    treename = 'tree'
    wf = MasterWorkflow(treename, 'materials.db')

    print('--- initial ls ----')
    wf.ls()

    nodes = wf.run_recursive()
    print('---- ls after run_recursive() ------')
    wf.ls()

    emt_elements = {'Al', 'Cu'}  # , 'Ag', 'Au', 'Ni', 'Pd', 'Pt'}

    class EMTMaterials:
        def __contains__(self, node):
            formula = set(node.toatoms().symbols)
            return formula <= emt_elements

    materials_selection = EMTMaterials()

    totree = nodes['totree']
    nodes = totree.run(selection=materials_selection)

    print('--- ls after explicit totree --------------')
    wf.ls()

    print('-------------------')

    wf.run_recursive()

    wf.ls()

    print('---------------------------')

    # wf.ls()


    task_key = [
        'emt_relax', 'Cu-167c7e7c3738b7428775babf52437ea5',
        'stiffness', 'strained_relaxations', '12m']

    task_name = '.'.join(task_key)
    print('name', task_name)

    task = wf[task_name]
    print(task)
