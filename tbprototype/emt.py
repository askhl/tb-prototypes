def do_niggli_reduce(material):
    from ase.build.tools import niggli_reduce
    atoms = material.toatoms().copy()
    niggli_reduce(atoms)
    return atoms


def with_emt(atoms):
    from asap3 import EMT
    atoms = atoms.copy()
    atoms.calc = EMT()
    return atoms


def _emt_relax(atoms, cellfilter, fmax=0.0001):
    from ase.optimize.bfgs import BFGS

    atoms = with_emt(atoms)
    with BFGS(cellfilter(atoms),
              trajectory=None) as opt:
        opt.run(fmax=fmax)

    atoms.calc = None
    return atoms


def emt_relax_cell(atoms, **kwargs):
    from ase.constraints import ExpCellFilter
    return _emt_relax(atoms, cellfilter=ExpCellFilter, **kwargs)


def emt_relax_fixedcell(atoms, **kwargs):
    return _emt_relax(atoms, cellfilter=lambda atoms: atoms, **kwargs)


def emt_singlepoint(atoms):
    atoms = with_emt(atoms)
    energy = atoms.get_potential_energy()
    forces = atoms.get_forces()
    stress = atoms.get_stress()
    return {'energy': energy,
            'forces': forces,
            'stress': stress}
