import numpy as np
from tbprototype.emt import (
    emt_relax_cell, emt_singlepoint, emt_relax_fixedcell)
from dataclasses import dataclass
import tbprototype.workflow as tb


@dataclass
class StrainID:
    i: int
    j: int
    sign: int
    strain_percent: float

    _ij_to_voigt = [[0, 5, 4],
                    [5, 1, 3],
                    [4, 3, 2]]

    @property
    def key(self):
        sign = {-1: 'm', 1: 'p'}[self.sign]
        return f'{self.i}{self.j}{sign}'

    @property
    def voigt_index(self):
        return self._ij_to_voigt[self.i][self.j]

    def strain(self, atoms):
        atoms = atoms.copy()
        strain_vv = np.eye(3)
        strain_vv[self.i, self.j] += self.sign * self.strain_percent / 100.0
        strain_vv = (strain_vv + strain_vv.T) / 2
        strained_cell_cv = atoms.cell @ strain_vv

        atoms.set_cell(strained_cell_cv, scale_atoms=True)
        return atoms

    def stiffness_contribution(self, stress):
        return self.sign * stress / (self.strain_percent * 0.02)

    @classmethod
    def disps(cls, strain_percent):
        for i, j in [(0, 0), (1, 1), (2, 2), (1, 2), (0, 2), (0, 1)]:
            for sign in [-1, 1]:
                yield cls(i, j, sign, strain_percent)


def emt_relax_strained(atoms, disp, fmax):
    strained_atoms = disp.strain(atoms)
    relaxed_strained_atoms = emt_relax_fixedcell(atoms=strained_atoms,
                                                 fmax=fmax)
    stress = emt_singlepoint(relaxed_strained_atoms)['stress']
    assert stress.shape == (6,)

    dstress = disp.stiffness_contribution(stress)
    return dstress


def stiffness(atoms):
    atoms = atoms.copy()

    fmax = 1e-8
    atoms = emt_relax_cell(atoms, fmax=fmax)

    strain_percent = 2.0
    # (Disps should depend on atoms' PBCs, whereas we assume ndim=3)

    stresses = []
    for disp in StrainID.disps(strain_percent):
        dstress = emt_relax_strained(atoms, disp, fmax)
        stresses.append((disp, dstress))

    stiffness_tensor = build_stiffness_tensor(stresses)
    return stiffness_tensor


class StiffnessWorkflow(tb.Workflow):
    def __init__(self, name, atoms, fmax=1e-8, strain_percent=2.0):
        self.atoms = atoms
        self.fmax = fmax
        self.strain_percent = strain_percent
        super().__init__(name)

    @tb.task(workertype='big')
    def relax0(self):
        return tb.Task(emt_relax_cell, atoms=self.atoms, fmax=self.fmax)

    @tb.subworkflow(eager=True)
    def strained_relaxations(self):
        # relax0 = self.atoms  # XXX should be self.relax0.output
        for disp in StrainID.disps(self.strain_percent):
            atoms = disp.strain(self.atoms)
            yield disp.key, tb.Task(emt_relax_strained, atoms=atoms,
                                    disp=disp, fmax=self.fmax)

    # @subworkflow(eager=True)
    # @inline_task(atoms=self.relax0.output,
    #              strain_percent=self.strain_percent.output)
    def strained_relaxations_experimental(self, atoms, strain_percent):
        # relax0 = self.atoms  # XXX should be self.relax0.output
        for disp in StrainID.disps(atoms.pbc, strain_percent):
            atoms = disp.strain(self.relax0.output)
            yield disp.key, tb.Task(emt_relax_strained, atoms=atoms,
                                    disp=disp, fmax=self.fmax)

    @tb.task(workertype='small')
    def stiffness_tensor(self):
        # XXX needs the disps as well
        return tb.Task(build_stiffness_tensor,
                       stresses=self.strained_relaxations)


def build_stiffness_tensor(stresses):
    stiffness_tensor = np.zeros((6, 6))
    for (disp, dstress) in stresses:
        stiffness_tensor[:, disp.voigt_index] += dstress

    import ase.units as u

    print('stiffness eV/Å³')
    print(stiffness_tensor)
    print()
    print('stiffness GPa')
    print(stiffness_tensor / u.GPa)
    print()
    stiffness_Nm2 = stiffness_tensor.copy()
    # What's with the 2**.5?
    stiffness_Nm2[3:, :] *= 2**.5
    stiffness_Nm2[:, 3:] *= 2**.5
    stiffness_Nm2 *= 10**30 / u.J
    print('stiffness N/m² (Mandel notation)')
    print(stiffness_Nm2)


def main():
    from ase.build import bulk
    np.set_printoptions(precision=3, suppress=True)
    atoms = bulk('Au')
    stiffness(atoms)


if __name__ == '__main__':
    main()
