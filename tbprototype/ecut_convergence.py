import numpy as np



class EnergyCalculation:
    def __init__(self, atoms, params):
        self.atoms = atoms
        self.params = params

    def __call__(self, *, ecut):
        from gpaw import GPAW
        atoms = self.atoms.copy()
        params = {**self.params, 'mode': {'name': 'pw', 'ecut': ecut}}
        atoms.calc = GPAW(**params)
        return atoms.get_potential_energy(force_consistent=True)


class Task:
    def __init__(self, target, **kwargs):
        self.target = target
        self.kwargs = kwargs

    def run(self):
        return self.target(**self.kwargs)

    def __repr__(self):
        return f'Task({self.target}, {self.kwargs})'


class EcutConvergence:
    def __init__(self, calculate, ecuts, tol):
        self.calculate = calculate
        self.ecuts = ecuts
        self.tol = tol

        self.tasks = []
        self.energies = []
        self.state = 'not done'
        self.workflow_output = None

    @property
    def counter(self):
        return len(self.tasks)

    def next_task(self):
        if self.counter != len(self.energies):
            raise NotReady

        try:
            ecut = self.ecuts[self.counter]
        except IndexError:
            raise DidNotConverge

        task = Task(self.calculate, ecut=ecut)
        self.tasks.append(task)
        return task

    def task_failed(self, task, error):
        assert len(self.energies) == len(self.tasks) - 1
        self.energies.append(None)

    def task_done(self, task, output):
        assert len(self.energies) == len(self.tasks) - 1
        self.energies.append(output)

        most_recent_energies = self.energies[-3:]
        if (
                len(most_recent_energies) == 3
                and None not in most_recent_energies
                and np.ptp(most_recent_energies) < self.tol):

            self.workflow_output = self.ecuts[self.counter]
            self.state = 'done'

    def is_workflow_done(self):
        return self.state == 'done'

    def run0000000000(self):
        for ecut in self.ecuts:
            energy = yield Task(self.calculate, ecut=ecut)
            self.energies.append(energy)

            deviation = np.ptp(self.energies[-3:])
            print(ecut, energy, deviation)

            if len(energies) >= 3 and deviation < self.tol:
                return

        raise RuntimeError('not converged')


class NotReady(Exception):
    pass


def main():
    from ase.build import bulk
    from gpaw import GPAW

    atoms = bulk('Pt')
    calculation = EnergyCalculation(
        atoms, params=dict(kpts={'density': 0.5}, txt=None))

    ecuts = np.logspace(np.log10(50), np.log10(2000), 40)

    workflow = EcutConvergence(calculation, ecuts, tol=0.05)

    while not workflow.is_workflow_done():
        tasks = []

        while True:
            try:
                task = workflow.next_task()
            except NotReady:
                break
            else:
                print('add task', task)
                tasks.append(task)

        for task in tasks:
            try:
                output = task.run()
            except Exception as ex:
                print('failed', task)
                workflow.task_failed(task, ex)
            else:
                print('done', task)
                workflow.task_done(task, output)

    print('final ecut', workflow.workflow_output)


def old_run_iterator(ecut_iterator):
    task = next(ecut_iterator)
    while True:
        output = task.run()
        try:
            task = ecut_iterator.send(output)
        except StopIteration:
            break


main()
