def subworkflow(meth):
    return meth

def task(meth):
    return meth

class ConvexHullWorkflow:
    external_db = tb.var()
    materials = tb.var()  # a kind of collection

    def _ehull(self, material):
        return tb.node('calculate_ehull',
                       databases=[self.external_db, self.materials],
                       material)

    @subworkflow
    def ehull(self):
        return tb.parametrize_task(self._ehull, self.materials)


class RelaxationWorkflow:
    material = tb.var()

    @subworkflow
    def relaxation(self):
        return relaxation_workflow(self.material)

    @subworkflow
    def phonons(self):
        return phonon_workflow(self.relaxation.output)

    @task
    def ehull_stuff(self):
        # XXX dynamical code somehow ????
        return EHullStuff(formula=self.material.formula,
                          energy=self.relaxation.output['energy'])


def build_ehull_db(selection):
    path = Path('ehull.db')
    ehull_db = MagicDictionary(path)
    for thing in selection:
        ehull_db[thing.formula] = thing.energy
    return path


class MasterWorkflow:
    materials_db = tb.var()
    external_convexhull_db = tb.var()

    def totree(self):
        ...  # something with self.materials_db

    def relaxationworkflow(self):
        return tb.parametrize(RelaxationWorkflow, self.totree)

    def internal_convex_hull_db(self):
        return tb.node('build_ehull_db',
                       Selection(self.materialworkflow.ehull_stuff))

    def _convex_hull(self, material):
        return ConvexHullWorkflow(
            external_db=self.external_convexhull_db,
            materials=self.internal_convex_hull_db,
            material=material)

    def convex_hull(self):
        return tb.parametrize(self._convex_hull,
                              Selection(self.relaxationworkflow.output))


def main():
    formula = 'GaAs'

    natoms = 2
    e0_gaas = -8.263  # this is for both atoms
    hform_gaas = -0.348  # this is per atom
    ref_energies_per_atom = {'Ga': -2.903, 'As': -4.663}
    ref_energy_sum = sum(ref_energies_per_atom.values())

    refs = {'Ga': Ref('Ga', 4, 0.0),
            'As': Ref('As', 2, 0.0)}

    thing = calculate_ehull(
        formula, e0_gaas, refs, ref_energies_per_atom)

    print(thing)

main()
